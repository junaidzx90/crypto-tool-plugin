<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.fiverr.com/junaidzx90
 * @since      1.0.0
 *
 * @package    Crypto_Tool
 * @subpackage Crypto_Tool/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Crypto_Tool
 * @subpackage Crypto_Tool/includes
 * @author     junaidzx90 <admin@easeare.com>
 */
class Crypto_Tool_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
