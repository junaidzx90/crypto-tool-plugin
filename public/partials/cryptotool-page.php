<?php get_header() ?>

<?php
wp_enqueue_style('crypto-tool');
wp_enqueue_script('crypto-tool');

wp_localize_script('crypto-tool', "ajax", array(
    'ajaxurl' 	=> admin_url('admin-ajax.php'),
    'nonce'		=> wp_create_nonce( 'crypto_nonce' ),
    'watermark' => get_option('watermarkonchart'),
    'color' => (get_option('toolgencolor') ? get_option('toolgencolor') : '#f7931a')
));
?>

<style>
    :root{
        --toolgeneral: <?php echo (get_option('toolgencolor') ? get_option('toolgencolor') : '#f7931a') ?>;
        --toolbg_color: <?php echo (get_option('toolbg_color') ? get_option('toolbg_color') : '#ffffff') ?>
    }
</style>
<div id="tool">
    <div class="collepsebutton">
        <span class="collepsebtn">
            <svg viewBox="64 64 896 896" focusable="false" data-icon="menu-unfold" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M408 442h480c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8H408c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8zm-8 204c0 4.4 3.6 8 8 8h480c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8H408c-4.4 0-8 3.6-8 8v56zm504-486H120c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8h784c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8zm0 632H120c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8h784c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8zM142.4 642.1L298.7 519a8.84 8.84 0 000-13.9L142.4 381.9c-5.8-4.6-14.4-.5-14.4 6.9v246.3a8.9 8.9 0 0014.4 7z"></path></svg>
        </span>
    </div>
    <div class="toolcontents">
        <div class="sidecats">
            <div class="charts">
                <h3 class="listtitle">Assets Coins</h3>
                <ul class="coin-options">
                    <li  data-type="Prices" data-end="/v1/metrics/addresses/min_point_zero_1_count" class="active">Addresses with Balance ≥ 0.01</li>
                    <li data-type="Volumen de transferencia" data-end="/v1/metrics/transactions/transfers_volume_sum" >Transfer Total Volume</li>
                    <li data-type="Tasas de transferencia" data-end="/v1/metrics/transactions/transfers_rate" >Transfer Rate</li>
                    <li data-type="Numero de transaccion" data-end="/v1/metrics/transactions/transfers_count" >Transfer Count</li>
                    <li data-type="Número de tamaño" data-end="/v1/metrics/transactions/size_sum" >Transaction Total Size</li>
                    <li data-type="Tarifas" data-end="/v1/metrics/transactions/rate" >Transaction Rate</li>
                    <li data-type="Recuentos de transacciones" data-end="/v1/metrics/transactions/count" >Transaction Count</li>
                    <li data-type="Direcciones" data-end="/v1/metrics/market/marketcap_usd" >Market Cap</li>
                    <li data-type="Direcciones activas" data-end="/v1/metrics/addresses/active_count" >Active Addresses</li>
                </ul>
            </div>

            <div id="minablecoins">
                <h3 class="listtitle">Minable Coins</h3>
                <ul class="minablecoins">
                    <li data-coin="dash">Dash</li>
                    <li data-coin="decred">Decred</li>
                    <li data-coin="dogecoin">DogeCoin</li>
                    <li data-coin="handshake">Handshake</li>
                    <li data-coin="helium">Helium</li>
                    <li data-coin="kadena">Kadena</li>
                    <li data-coin="lbry-credits">Library</li>
                    <li data-coin="nervos-network">Nervos Network</li>
                    <li data-coin="ethereum-classic">Ethereum Classic</li>
                    <li data-coin="zcash">ZCash</li>
                </ul>
            </div>
        </div>

        <div class="chartviews">
            <div class="filtersCoin">
                <div class="glnode filter">
                    <input checked type="radio" name="coinfilter" id="ethcoin" value="eth">
                    <label for="ethcoin">ETH</label>
                </div>
                <div class="glnode filter">
                    <input type="radio" name="coinfilter" id="ltccoin" value="ltc">
                    <label for="ltccoin">LTC</label>
                </div>
                <div class="glnode filter">
                    <input type="radio" name="coinfilter" id="btccoin" value="btc">
                    <label for="btccoin">BTC</label>
                </div>

                <div class="coins filter">
                    <input checked type="radio" name="coinsFilter" id="prices" value="prices">
                    <label for="prices">Prices</label>
                </div>
                <div class="coins filter">
                    <input type="radio" name="coinsFilter" id="marketcap" value="marketcap">
                    <label for="marketcap">Market Cap</label>
                </div>
                <div class="coins filter">
                    <input type="radio" name="coinsFilter" id="marketvol" value="marketvol">
                    <label for="marketvol">Market Volume</label>
                </div>
            </div>

            <div id="toolChart" class="toolChart">
                <!--  -->
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>