<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.fiverr.com/junaidzx90
 * @since      1.0.0
 *
 * @package    Crypto_Tool
 * @subpackage Crypto_Tool/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Crypto_Tool
 * @subpackage Crypto_Tool/public
 * @author     junaidzx90 <admin@easeare.com>
 */
class Crypto_Tool_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Crypto_Tool_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Crypto_Tool_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_register_style( 'crypto-tool', plugin_dir_url( __FILE__ ) . 'css/crypto-tool-archive.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Crypto_Tool_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Crypto_Tool_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_register_script( 'chartjs', plugin_dir_url( __FILE__ ) . 'js/chart.min.js', array(), $this->version, true );
		wp_register_script( 'crypto-tool', plugin_dir_url( __FILE__ ) . 'js/crypto-tool-archive.js', array( 'chartjs' ), $this->version, true );
	}

	public function cryptotools_page($templates){ 
        $templates['cryptotool'] = 'Tool';
        return $templates;
    }

	// Include competitions archive page
	function cryptotools_templates( $template ) {
		// For custom archive page
		if ( get_page_template_slug() === 'cryptotool' ) {
			$theme_files = array('cryptotool-page.php', plugin_dir_path( __FILE__ ).'partials/cryptotool-page.php');
			$exists_in_theme = locate_template($theme_files, false);
			if ( $exists_in_theme != '' ) {
				return $exists_in_theme;
			} else {
				return  plugin_dir_path( __FILE__ ). 'partials/cryptotool-page.php';
			}
		}
		if ($template == '') {
			throw new \Exception('No template found');
		}
		return $template;
	}

	function get_databyapi($endpoint, $params){
		$gapi = null;
		if(get_option('glassnode_api')){
			$gapi = get_option('glassnode_api');
		}
		$url = "https://api.glassnode.com";
		$response = wp_remote_get( "$url$endpoint?api_key=$gapi$params", array('mode' => 'no-cors') );
		$retrieve_body = wp_remote_retrieve_body($response);
		$retrieve_body = json_decode($retrieve_body);
		return $retrieve_body;
	}

	function get_chartdata(){
		if ( ! wp_verify_nonce( $_GET['nonce'], 'crypto_nonce' ) ) {
			die ( 'Hey! What are you doing?');
		}
		
		$endpoint = null;
		if(isset($_GET['endpoint']) && !empty($_GET['endpoint'])){
			$endpoint = $_GET['endpoint'];
		}
		
		$coin = null;
		if(isset($_GET['coin']) && !empty($_GET['coin'])){
			$coin = $_GET['coin'];
		}
		
		$data = array();
		if($endpoint && $coin){
			$data = $this->get_databyapi("$endpoint", "&a=$coin&timestamp_format=unix");
		}
		
		echo json_encode(array('success' => $data));
		die;
	}

	function get_chartdatacoingeeko(){
		if ( ! wp_verify_nonce( $_GET['nonce'], 'crypto_nonce' ) ) {
			die ( 'Hey! What are you doing?');
		}
		
		$coin = null;
		if(isset($_GET['coin']) && !empty($_GET['coin'])){
			$coin = $_GET['coin'];
		}
		
		$data = array();
		if($coin){
			$url = "https://api.coingecko.com/api/v3/coins/$coin/market_chart?vs_currency=usd&days=max";
			$response = wp_remote_get( $url );
			$retrieve_body = wp_remote_retrieve_body($response);
			$retrieve_body = json_decode($retrieve_body);
			$data = $retrieve_body;
		}
		
		echo json_encode(array('success' => $data));
		die;
	}
}
