jQuery(document).ready(function ($) {
    if (matchMedia('only screen and (max-width: 768px)').matches) {
        // let sidebar = $('.sidecats');
        // let chartviews = $('.chartviews');
        // sidebar.addClass("sidetoggle");
        // chartviews.addClass("charttoggle");
        let msg = '<h1 class="notsupported">Para usar esta herramienta puede acceder a través de un ordenador.</h1>';
        $('#tool').html(msg);
    } else {
    
        function timeConverter(UNIX_timestamp, multi = false) {
            var a = new Date(UNIX_timestamp);
            if (multi) {
                a = new Date(UNIX_timestamp * 1000);
            }
            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var year = a.getFullYear();
            var month = months[a.getMonth()];
            var date = a.getDate();
            var time = date + ' ' + month + ' ' + year;
            return time;
        }
    
        const chart_watermark = {
            id: 'marketchart',
            afterDraw: (chart) => {
                const image = new Image();
                image.src = ajax.watermark;
                if (image.complete) {
                    const image_height = 100; // Or you can use image.naturalHeight;
                    const image_width = 200; // Or you can use image.naturalWidth;
                    const ctx = chart.ctx;
                    const x = chart.chartArea.width - image_width;
                    const y = chart.chartArea.height - image_height;
                    ctx.globalAlpha = 0.10;
                    ctx.drawImage(image, x, y, image_width, image_height);
                    ctx.globalAlpha = 1;
                } else {
                    image.onload = () => chart.draw();
                }
            }
        };

        // Register or Apply the plugin globally
        Chart.register(chart_watermark);

        function initialChart(data, types) {
            let totalDuration1 = 1000;
            let delayBetweenPoints1 = totalDuration1 / data.length;
            let previousY = (ctx) => ctx.index === 0 ? ctx.chart.scales.y.getPixelForValue(100) : ctx.chart.getDatasetMeta(ctx.datasetIndex).data[ctx.index - 1].getProps(['y'], true).y;
            let animation = {
                x: {
                    type: 'number',
                    easing: 'linear',
                    duration: delayBetweenPoints1,
                    from: NaN, // the point is initially skipped
                    delay(ctx) {
                        if (ctx.type !== 'data' || ctx.xStarted) {
                            return 0;
                        }
                        ctx.xStarted = true;
                        return ctx.index * delayBetweenPoints1;
                    }
                },
                y: {
                    type: 'number',
                    easing: 'linear',
                    duration: delayBetweenPoints1,
                    from: previousY,
                    delay(ctx) {
                        if (ctx.type !== 'data' || ctx.yStarted) {
                            return 0;
                        }
                        ctx.yStarted = true;
                        return ctx.index * delayBetweenPoints1;
                    }
                }
            };
    
            const config = {
                type: 'line',
                data: {
                    labels: data.map((coin) => {
                        return timeConverter(coin['t'], true)
                    }),
                    datasets: [
                        {
                            label: types,
                            data: data.map((coin) => coin['v']),
                            borderColor: [
                                ajax.color,
                            ],
                            borderWidth: 1,
                            radius: 0,
                        },
                    ]
                },
                options: {
                    animation,
                    interaction: {
                        intersect: false
                    },
                    plugins: {
                        legend: false,
                    },
                }
            };

            $('#toolChart').html(`<canvas id="chart"></canvas>`);

            new Chart(
                document.getElementById(`chart`),
                config
            );
        }

        function coinChart(dataArr, filter, label) {
            let data = [];
            if (filter == 'prices') {
                data = dataArr.prices;
            }
            if (filter == 'marketcap') {
                data = dataArr.market_caps;
            }
            if (filter == 'marketvol') {
                data = dataArr.total_volumes;
            }
        
            let totalDuration1 = 1000;
            let delayBetweenPoints1 = totalDuration1 / data.length;
            let previousY = (ctx) => ctx.index === 0 ? ctx.chart.scales.y.getPixelForValue(100) : ctx.chart.getDatasetMeta(ctx.datasetIndex).data[ctx.index - 1].getProps(['y'], true).y;
            let animation = {
                x: {
                    type: 'number',
                    easing: 'linear',
                    duration: delayBetweenPoints1,
                    from: NaN, // the point is initially skipped
                    delay(ctx) {
                        if (ctx.type !== 'data' || ctx.xStarted) {
                            return 0;
                        }
                        ctx.xStarted = true;
                        return ctx.index * delayBetweenPoints1;
                    }
                },
                y: {
                    type: 'number',
                    easing: 'linear',
                    duration: delayBetweenPoints1,
                    from: previousY,
                    delay(ctx) {
                        if (ctx.type !== 'data' || ctx.yStarted) {
                            return 0;
                        }
                        ctx.yStarted = true;
                        return ctx.index * delayBetweenPoints1;
                    }
                }
            };
    
            const config = {
                type: 'line',
                data: {
                    labels: data.map((coin) => {
                        return timeConverter(coin[0])
                    }),
                    datasets: [
                        {
                            label: label,
                            data: data.map((coin) => coin[1]),
                            borderColor: [
                                ajax.color,
                            ],
                            borderWidth: 1,
                            radius: 0,
                        },
                    ]
                },
                options: {
                    animation,
                    interaction: {
                        intersect: false
                    },
                    plugins: {
                        legend: false,
                    },
                }
            };

            $('#toolChart').html(`<canvas id="chart"></canvas>`);

            new Chart(
                document.getElementById(`chart`),
                config
            );
        }

        function loaderAnimate() {
            $('body').append(`<div class="loaderbody">
            <span class="spinner">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </span>
        </div>`);
        }

        let glassnode = true;
        let endpoint = '/v1/metrics/addresses/min_point_zero_1_count';
        let types = 'Direcciones';

        function filterVisibility() {
            if (!glassnode) {
                $('.filter.glnode').hide();
                $('.filter.coins').show();
            } else {
                $('.filter.glnode').show();
                $('.filter.coins').hide();
            }
        }

        let filter = 'eth';

        $.ajax({
            type: "get",
            url: ajax.ajaxurl,
            data: {
                action: "get_chartdata",
                coin: filter,
                endpoint: endpoint,
                nonce: ajax.nonce,
            },
            beforeSend: () => {
                loaderAnimate()
            },
            dataType: "json",
            success: function (response) {
                $('.loaderbody').remove();
                if (response.success) {
                    let data = response.success;
                    initialChart(data, types);
                    filterVisibility();
                }
            }
        });

        $('.coin-options li').on("click", function () { // Glassnode request
            let btn = $(this);
            types = $(this).attr('data-type');
            endpoint = $(this).attr('data-end');
            $.ajax({
                type: "get",
                url: ajax.ajaxurl,
                data: {
                    action: "get_chartdata",
                    coin: filter,
                    endpoint: endpoint,
                    nonce: ajax.nonce,
                },
                beforeSend: () => {
                    loaderAnimate()
                },
                dataType: "json",
                success: function (response) {
                    $('.loaderbody').remove();
                    if (response.success) {
                        glassnode = true;
                        let data = response.success;
                        initialChart(data, types);
                        filterVisibility();
                        btn.siblings("li").removeClass("active");
                        $('.minablecoins li.active').removeClass("active");
                        btn.addClass("active");
                    }
                }
            });
        });

        let coinid = 'dash';
        let filter2 = 'prices';
        $('.minablecoins li').on("click", function () { // Coingeeko request
            let btn = $(this);
            coinid = $(this).attr("data-coin");
            $.ajax({
                type: "get",
                url: ajax.ajaxurl,
                data: {
                    action: "get_chartdatacoingeeko",
                    coin: coinid,
                    nonce: ajax.nonce,
                },
                beforeSend: () => {
                    loaderAnimate()
                },
                dataType: "json",
                success: function (response) {
                    $('.loaderbody').remove();
                    if (response.success) {
                        glassnode = false;
                        let data = response.success;
                        let lbs = '';
                        switch (filter2) {
                            case 'prices':
                                lbs = 'Direcciones';
                                break;
                            case 'marketcap':
                                lbs = 'Tapa del mercado';
                                break;
                            case 'marketvol':
                                lbs = 'Volúmenes';
                                break;
                    
                            default:
                                break;
                        }
                        coinChart(data, filter2, lbs);
                        filterVisibility();
                        $('.coin-options li.active').removeClass("active");
                        btn.siblings("li").removeClass("active");
                        btn.addClass("active");
                    }
                }
            });
        });

        $('input[name="coinfilter"]').on("change", function () { // Glassnode filters
            filter = $(this).val();
            $.ajax({
                type: "get",
                url: ajax.ajaxurl,
                data: {
                    action: "get_chartdata",
                    coin: filter,
                    endpoint: endpoint,
                    currency: "usd",
                    nonce: ajax.nonce,
                },
                beforeSend: () => {
                    loaderAnimate();
                },
                dataType: "json",
                success: function (response) {
                    $('.loaderbody').remove();
                    let data = [];
                    if (response.success) {
                        data = response.success
                    }
                    initialChart(data, types);
                    filterVisibility();
                }
            });
        });
    
        $('input[name="coinsFilter"]').on("change", function () { // CoinGeeko filters pass selected filter data to the chart
            filter2 = $(this).val();
            $.ajax({
                type: "get",
                url: ajax.ajaxurl,
                data: {
                    action: "get_chartdatacoingeeko",
                    coin: coinid,
                    nonce: ajax.nonce,
                },
                beforeSend: () => {
                    loaderAnimate();
                },
                dataType: "json",
                success: function (response) {
                    $('.loaderbody').remove();
                    if (response.success) {
                        let data = response.success;

                        let lbs = '';
                        switch (filter2) {
                            case 'prices':
                                lbs = 'Direcciones';
                                break;
                            case 'marketcap':
                                lbs = 'Tapa del mercado';
                                break;
                            case 'marketvol':
                                lbs = 'Volúmenes';
                                break;
                    
                            default:
                                break;
                        }
                        coinChart(data, filter2, lbs);
                        filterVisibility();
                        $('.coin-options li.active').removeClass("active");
                    }
                }
            });
        });

        // Collepse toggle button
        $('.collepsebtn').on("click", function () {
            let sidebar = $('.sidecats');
            let chartviews = $('.chartviews');
            sidebar.toggleClass("sidetoggle");
            chartviews.toggleClass("charttoggle");
        });
    }
});