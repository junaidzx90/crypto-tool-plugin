<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.fiverr.com/junaidzx90
 * @since      1.0.0
 *
 * @package    Crypto_Tool
 * @subpackage Crypto_Tool/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Crypto_Tool
 * @subpackage Crypto_Tool/admin
 * @author     junaidzx90 <admin@easeare.com>
 */
class Crypto_Tool_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Crypto_Tool_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Crypto_Tool_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/crypto-tool-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Crypto_Tool_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Crypto_Tool_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/crypto-tool-admin.js', array( 'jquery' ), $this->version, false );

	}

	function admin_menupage(){
		add_menu_page( 'Crypto tool', 'Crypto tool', 'manage_options', 'crypto-tool', [$this, 'cryptotool_menupage'], 'dashicons-money-alt', 45 );

		add_settings_section( 'cryptotool_settings_section', '', '', 'cryptotool_settings_page' );

		// Glassnode API
		add_settings_field( 'glassnode_api', 'Glassnode API', [$this, 'glassnode_api_cb'], 'cryptotool_settings_page', 'cryptotool_settings_section');
		register_setting( 'cryptotool_settings_section', 'glassnode_api');
		// Chart Watermark
		add_settings_field( 'watermarkonchart', 'Chart Watermark', [$this, 'watermarkonchart_cb'], 'cryptotool_settings_page', 'cryptotool_settings_section');
		register_setting( 'cryptotool_settings_section', 'watermarkonchart');
		
		// General-color
		add_settings_field( 'toolgencolor', 'General color', [$this, 'toolgencolor_cb'], 'cryptotool_settings_page', 'cryptotool_settings_section');
		register_setting( 'cryptotool_settings_section', 'toolgencolor');
		// Background-color
		add_settings_field( 'toolbg_color', 'Background color', [$this, 'toolbg_color_cb'], 'cryptotool_settings_page', 'cryptotool_settings_section');
		register_setting( 'cryptotool_settings_section', 'toolbg_color');
	}

	function glassnode_api_cb(){
		echo '<input type="text" class="widefat" name="glassnode_api" placeholder="API Key" value="'.get_option('glassnode_api').'">';
	}

	// Menupage callback
	function cryptotool_menupage(){
		echo '<h3>Settings</h3><hr>';
		echo '<div class="cryptotool_content">';
		echo '<form style="width: 50%" method="post" action="options.php">';
		echo '<table class="widefat">';
		settings_fields( 'cryptotool_settings_section' );
		do_settings_fields( 'cryptotool_settings_page', 'cryptotool_settings_section' );
		echo '</table>';
		submit_button();
		echo '</form>';
		echo '</div>';
	}

	// watermarkonchart_cb
	function watermarkonchart_cb(){
		echo '<input type="url" class="widefat" name="watermarkonchart" placeholder="Logo Url" value="'.get_option('watermarkonchart').'" id="watermarkonchart">';
	}
	//Header Image
	function tool_headrimg_cb(){
		echo '<input type="url" class="widefat" name="tool_headrimg" placeholder="Image URL" value="'.get_option('tool_headrimg').'" id="tool_headrimg">';
	}

	// General color
	function toolgencolor_cb(){
		echo '<input type="color" name="toolgencolor" value="'.(get_option('toolgencolor') ? get_option('toolgencolor') : '#f7931a').'">';
	}

	// Bg color
	function toolbg_color_cb(){
		echo '<input type="color" name="toolbg_color" value="'.(get_option('toolbg_color') ? get_option('toolbg_color') : '#ffffff').'">';
	}

}
